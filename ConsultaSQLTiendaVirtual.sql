-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tiendavirtual
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tiendavirtual
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendavirtual` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `tiendavirtual` ;

-- -----------------------------------------------------
-- Table `tiendavirtual`.`inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual`.`inventario` (
  `id_inventario` INT NOT NULL AUTO_INCREMENT,
  `cantidad_disponible` INT NOT NULL,
  PRIMARY KEY (`id_inventario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendavirtual`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `nombre_usuario` VARCHAR(45) NOT NULL,
  `contraseña` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendavirtual`.`productocomprado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual`.`productocomprado` (
  `id_productocomprado` INT NOT NULL AUTO_INCREMENT,
  `nombre_producto` VARCHAR(25) NOT NULL,
  `fecha_compra` DATETIME NOT NULL,
  `procedencia_marca` VARCHAR(25) NOT NULL,
  `precio` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `fecha_vencimiento` DATETIME NOT NULL,
  `tipo_almacenamiento` VARCHAR(25) NOT NULL,
  `id_usuariocompra` INT NOT NULL,
  PRIMARY KEY (`id_productocomprado`),
  INDEX `id_usuariocompra_idx` (`id_usuariocompra` ASC) VISIBLE,
  CONSTRAINT `fk_idproducto`
    FOREIGN KEY (`id_productocomprado`)
    REFERENCES `tiendavirtual`.`inventario` (`id_inventario`),
  CONSTRAINT `id_usuariocompra`
    FOREIGN KEY (`id_usuariocompra`)
    REFERENCES `tiendavirtual`.`usuario` (`id_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendavirtual`.`productovendido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual`.`productovendido` (
  `id_productovendido` INT NOT NULL AUTO_INCREMENT,
  `nombre_producto` VARCHAR(25) NOT NULL,
  `fecha_venta` DATETIME NOT NULL,
  `precio` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `id_usuariovende` INT NOT NULL,
  `id_productoalmacenado` INT NOT NULL,
  PRIMARY KEY (`id_productovendido`),
  INDEX `id_usuariovende_idx` (`id_usuariovende` ASC) VISIBLE,
  INDEX `fk_id_inventario_idx` (`id_productoalmacenado` ASC) VISIBLE,
  CONSTRAINT `fk_id_inventario`
    FOREIGN KEY (`id_productoalmacenado`)
    REFERENCES `tiendavirtual`.`inventario` (`id_inventario`),
  CONSTRAINT `id_usuariovende`
    FOREIGN KEY (`id_usuariovende`)
    REFERENCES `tiendavirtual`.`usuario` (`id_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
